const axios = require('axios').default;

export const example = async () => {
    try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/posts/1');
       return response.data
      } catch (error) {
        console.error(error);
      }
}